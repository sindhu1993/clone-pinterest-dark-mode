import 'package:card_swiper/card_swiper.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../themes.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final List<String> imageList = [
    "https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80",
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];

  String gambar =
      "https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80";

  @override
  Widget build(BuildContext context) {
    // widget search
    Widget search() {
      return Container(
        margin: EdgeInsets.all(10),
        height: 30,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50), color: Colors.grey[800]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              transform: Matrix4.translationValues(4, 0, 0),
              child: Icon(
                Icons.search,
                color: Colors.white,
                size: 25.0,
              ),
            ),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  transform: Matrix4.translationValues(3, 2, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      hintText: 'Cari Ide',
                      hintStyle: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[200]),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              transform: Matrix4.translationValues(-9, 0, 0),
              child: Icon(
                Icons.camera_alt,
                color: Colors.white,
                size: 25.0,
              ),
            ),
          ],
        ),
      );
    }
    // widget search

    // widget carousel
    Widget carouselTop() {
      return Container(
        // margin: EdgeInsets.all(20),
        color: Color.fromARGB(174, 153, 153, 153),
        height: 250,
        width: MediaQuery.of(context).size.width,
        child: Swiper(
          autoplay: true,
          itemBuilder: (BuildContext context, int index) {
            return Stack(
              children: [
                Container(
                  height: 280,
                  child: Opacity(
                    opacity: 0.2,
                    child: Image.network(
                      imageList[index],
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    'untuk keluarga',
                    style: TextStyle(color: Colors.white, fontSize: 8),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 29),
                  alignment: Alignment.center,
                  child: Text(
                    'Insipirasi Keluarga',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            );
          },
          itemCount: 3,
          pagination: SwiperPagination(
            builder: DotSwiperPaginationBuilder(
              color: Colors.grey[300],
              activeColor: Colors.white,
            ),
          ),
          control: SwiperControl(
            color: Colors.transparent,
          ),
        ),
      );
    }
    // widget carousel

    // widget title
    Widget titleList(String title, double x, double y, double z) {
      return Container(
        margin: EdgeInsets.all(8),
        // height: 80,
        transform: Matrix4.translationValues(x, y, z),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
                fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ),
      );
    }
    // widget title

    // Widget Card buat anda
    Widget cardBuatAnda() {
      return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            // color: Colors.white
          ),
          height: 80,
          width: MediaQuery.of(context).size.width,
          // margin: EdgeInsets.all(8),
          transform: Matrix4.translationValues(0, 10, 0),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Stack(
                children: [
                  Container(
                    height: 280,
                    width: MediaQuery.of(context).size.width * 0.45,
                    margin: EdgeInsets.all(8),
                    child: Opacity(
                      opacity: 0.2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          gambar,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    transform: Matrix4.translationValues(40, 40, 0),
                    child: Text(
                      'untuk keluarga',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                ],
              ),
              Stack(
                children: [
                  Container(
                    height: 280,
                    width: MediaQuery.of(context).size.width * 0.45,
                    margin: EdgeInsets.all(8),
                    child: Opacity(
                      opacity: 0.2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          gambar,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    transform: Matrix4.translationValues(40, 40, 0),
                    child: Text(
                      'untuk keluarga',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                ],
              ),
            ],
          ));
    }
    // Widget Card buat anda

    // Widget Card populer pinterest anda
    Widget cardPopulerPinterest(String title1, String title2) {
      return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            // color: Colors.white
          ),
          height: 80,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.only(bottom: 8),
          transform: Matrix4.translationValues(0, 10, 0),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Stack(
                children: [
                  Container(
                    height: 280,
                    width: MediaQuery.of(context).size.width * 0.45,
                    margin: EdgeInsets.all(8),
                    child: Opacity(
                      opacity: 0.2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          gambar,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 100,
                    transform: Matrix4.translationValues(40, 40, 0),
                    child: Text(
                      title1,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                ],
              ),
              Stack(
                children: [
                  Container(
                    height: 280,
                    width: MediaQuery.of(context).size.width * 0.45,
                    margin: EdgeInsets.all(8),
                    child: Opacity(
                      opacity: 0.2,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          gambar,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 100,
                    transform: Matrix4.translationValues(40, 40, 0),
                    child: Text(
                      title2,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                ],
              ),
            ],
          ));
    }
    // Widget Card populer pinterest anda

    // Widget card kreator
    Widget cardKreator() {
      return Container(
        height: 120,
        margin: EdgeInsets.only(left: 10),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 8),
                  height: 100,
                  width: 60,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      gambar,
                      height: 100,
                      width: 60,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: 30,
                  padding: EdgeInsets.all(3),
                  transform: Matrix4.translationValues(2, 5, 0),
                  // color: Colors.grey[200],
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.black38,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.copy,
                        color: Colors.white,
                        size: 12,
                      ),
                      Text(
                        '6',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 60,
                  height: 30,
                  transform: Matrix4.translationValues(3, 85, 0),
                  // color: Colors.white,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      shape: BoxShape.circle,
                      image: DecorationImage(image: NetworkImage(gambar))),
                  // child: Image.network(gambar),
                )
              ],
            ),
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 8),
                  height: 100,
                  width: 60,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      gambar,
                      height: 100,
                      width: 60,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: 30,
                  padding: EdgeInsets.all(3),
                  transform: Matrix4.translationValues(2, 5, 0),
                  // color: Colors.grey[200],
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.black38,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.copy,
                        color: Colors.white,
                        size: 12,
                      ),
                      Text(
                        '6',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 60,
                  height: 30,
                  transform: Matrix4.translationValues(3, 85, 0),
                  // color: Colors.white,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      shape: BoxShape.circle,
                      image: DecorationImage(image: NetworkImage(gambar))),
                  // child: Image.network(gambar),
                )
              ],
            ),
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 8),
                  height: 100,
                  width: 60,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      gambar,
                      height: 100,
                      width: 60,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: 30,
                  padding: EdgeInsets.all(3),
                  transform: Matrix4.translationValues(2, 5, 0),
                  // color: Colors.grey[200],
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.black38,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.copy,
                        color: Colors.white,
                        size: 12,
                      ),
                      Text(
                        '6',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 60,
                  height: 30,
                  transform: Matrix4.translationValues(3, 85, 0),
                  // color: Colors.white,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      shape: BoxShape.circle,
                      image: DecorationImage(image: NetworkImage(gambar))),
                  // child: Image.network(gambar),
                )
              ],
            ),
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 8),
                  height: 100,
                  width: 60,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      gambar,
                      height: 100,
                      width: 60,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: 30,
                  padding: EdgeInsets.all(3),
                  transform: Matrix4.translationValues(2, 5, 0),
                  // color: Colors.grey[200],
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.black38,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.copy,
                        color: Colors.white,
                        size: 12,
                      ),
                      Text(
                        '6',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 60,
                  height: 30,
                  transform: Matrix4.translationValues(3, 85, 0),
                  // color: Colors.white,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      shape: BoxShape.circle,
                      image: DecorationImage(image: NetworkImage(gambar))),
                  // child: Image.network(gambar),
                )
              ],
            ),
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 8),
                  height: 100,
                  width: 60,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      gambar,
                      height: 100,
                      width: 60,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: 30,
                  padding: EdgeInsets.all(3),
                  transform: Matrix4.translationValues(2, 5, 0),
                  // color: Colors.grey[200],
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.black38,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.copy,
                        color: Colors.white,
                        size: 12,
                      ),
                      Text(
                        '6',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: 60,
                  height: 30,
                  transform: Matrix4.translationValues(3, 85, 0),
                  // color: Colors.white,
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      shape: BoxShape.circle,
                      image: DecorationImage(image: NetworkImage(gambar))),
                  // child: Image.network(gambar),
                )
              ],
            ),
            Stack(
              children: [
                Container(
                transform: Matrix4.translationValues(0, 30, 0),
                width: 100,
                height: 45,
                margin: EdgeInsets.only(left: 10,right: 10),
                child: TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: Color.fromARGB(255, 146, 146, 146),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    "Lihat Semua",
                    style: TextStyle(
                      color: Color(0xffffffff),
                    ),
                  ),
                ),
                ),
              ],
            ),
          ],
        ),
      );
    }
    // Widget card kreator

    return Scaffold(
      backgroundColor: backgroundColorTheme,
      resizeToAvoidBottomInset: true,
      // appBar: myAppbar,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 12.0),
              child: Column(
                children: [
                  carouselTop(),
                  titleList('Ide-ide dari kreator', 0, 0, 0),
                  cardKreator(),
                  titleList('Ide-ide buat Anda', 0, 10, 0),
                  cardBuatAnda(),
                  titleList('Popular di Pinterest', 0, 20, 0),
                  cardPopulerPinterest(
                      'Partisi ruang tamu minimalis', 'jadwal lucu'),
                  cardPopulerPinterest('Desain Furniture', 'Paper Duck'),
                  cardPopulerPinterest('Header Twitter', 'Minuman alkohol'),
                ],
              ),
            ),
          ),
          Positioned(
            top: 24.0,
            left: 0,
            right: 0,
            child: search(),
          )
        ],
      ),
    );
  }
}
