import 'package:clone_pinterest_flutter/pages/tonton.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../themes.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentTap = 1;

  @override
  Widget build(BuildContext context) {
    // widget myappbar
    final myAppbar = AppBar(
      elevation: 0,
      backgroundColor: backgroundColorTheme,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              if (currentTap > 1) {
                setState(() {
                  currentTap = 1;
                });
              }
              // print(currentTap);
            },
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Container(
                    child: Text(
                      'Jelajahi',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    width: 50.0,
                    height: 5.0,
                    decoration: ShapeDecoration(
                        shape: StadiumBorder(),
                        color: (currentTap == 1)
                            ? Colors.white
                            : backgroundColorTheme),
                    child: Text(''),
                  ),
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              if (currentTap == 1) {
                setState(() {
                  currentTap = 2;
                });
              }
              // print(currentTap);
            },
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: Text(
                      'Tonton',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  width: 50.0,
                  height: 5.0,
                  decoration: ShapeDecoration(
                      shape: StadiumBorder(),
                      color: (currentTap == 2)
                          ? Colors.white
                          : backgroundColorTheme),
                  child: Text(''),
                )
              ],
            ),
          ),
        ],
      ),
    );
    // widget myappbar

    // widget body Jelajahi
    Widget bodyJelajahi() {
      return Container(
        margin: EdgeInsets.only(
            top: defaultMargin, left: defaultMargin, right: defaultMargin),
        child: StaggeredGridView.countBuilder(
          shrinkWrap: true,
          primary: false,
          crossAxisCount: 4,
          itemCount: 10,
          itemBuilder: (BuildContext context, int index) => Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Column(
              children: [
                Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.asset('car.jpg', fit: BoxFit.cover),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 4,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            'Car Lamborghini Supercar',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: MediaQuery.of(context).size.width / 30
                            ),
                          ),
                        ),
                      ),
                    ),
                    
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Icon(
                          Icons.settings,
                          color: Colors.white,
                          size: 20.0,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          staggeredTileBuilder: (int index) => StaggeredTile.fit(2),
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        ),
      );
    }
    // widget body Jelajahi

    // widget body Tonton
    Widget bodyTonton() {
      return Container(
        margin: EdgeInsets.only(
            top: defaultMargin, left: defaultMargin, right: defaultMargin),
        child: StaggeredGridView.countBuilder(
          shrinkWrap: true,
          primary: false,
          crossAxisCount: 4,
          itemCount: 10,
          itemBuilder: (BuildContext context, int index) => Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Column(
              children: [
                Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.asset('holland.jpg', fit: BoxFit.cover),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    
                    Expanded(
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            'Holland Beautiful place for us',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.white,
                               fontSize: MediaQuery.of(context).size.width / 30
                            ),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Icon(
                        Icons.settings,
                        color: Colors.white,
                        size: 20.0,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          staggeredTileBuilder: (int index) => StaggeredTile.fit(2),
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
        ),
      );
    }
    // widget body Tonton

    return Scaffold(
        backgroundColor: backgroundColorTheme,
        resizeToAvoidBottomInset: true,
        appBar: myAppbar,
        body: (currentTap == 1) ? bodyJelajahi() : bodyTonton());
  }
}
