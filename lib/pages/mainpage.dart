import 'package:clone_pinterest_flutter/pages/chat.dart';
import 'package:clone_pinterest_flutter/pages/home.dart';
import 'package:clone_pinterest_flutter/pages/profile.dart';
import 'package:clone_pinterest_flutter/pages/search.dart';
import 'package:clone_pinterest_flutter/themes.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    // widget body start
    Widget body() {
      switch (currentIndex) {
        case 0:
          // return HomePage();
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: HomePage(),
          );
        case 1:
          // return SearchPage();
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: SearchPage(),
          );
        case 2:
          // return ChatPage();
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: ChatPage(),
          );
        case 3:
          // return ProfilePage();
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: ProfilePage(),
          );
        default:
          // return HomePage();
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: HomePage(),
          );
      }
    }
    // widget body end

    // Widget custom bottom nav start
    Widget customBottomNav() {
      return BottomAppBar(
        notchMargin: 12,
        elevation: 0,
        clipBehavior: Clip.antiAlias,
        child: BottomNavigationBar(
          elevation: 0,
          backgroundColor: backgroundColorTheme,
          currentIndex: currentIndex,
          onTap: (value) {
            // currentIndex = value;
            setState(() {
              currentIndex = value;
            });
          },
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(
                  top: 20,
                  bottom: 10,
                ),
                child: Icon(
                  Icons.home,
                  color: (currentIndex == 0) ? Colors.white : Colors.grey,
                  size: 30.0,
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(
                  top: 20,
                  bottom: 10,
                ),
                child: Icon(
                  Icons.search,
                  color: (currentIndex == 1) ? Colors.white : Colors.grey,
                  size: 30.0,
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(
                  top: 20,
                  bottom: 10,
                ),
                child: Icon(
                  Icons.chat_sharp,
                  color: (currentIndex == 2) ? Colors.white : Colors.grey,
                  size: 30.0,
                ),
              ),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Container(
                margin: EdgeInsets.only(
                  top: 20,
                  bottom: 10,
                ),
                child: Icon(
                  Icons.person,
                  color: (currentIndex == 3) ? Colors.white : Colors.grey,
                  size: 30.0,
                ),
              ),
              label: '',
            ),
          ],
        ),
      );
    }
    // Widget custom bottom nav end

    return Scaffold(
      backgroundColor: backgroundColorTheme,
      bottomNavigationBar: customBottomNav(),
      body: body(),
    );
  }
}
